package com.blocksglobal.lumen.button.object;

/**
 * Created by nanik on 31/03/16.
 */
public class BluetoothDeviceObject {
    String   btName;
    String   btAddress;

    public String getBtName() {
        return btName;
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }

    public String getBtAddress() {
        return btAddress;
    }

    public void setBtAddress(String btAddress) {
        this.btAddress = btAddress;
    }
}
