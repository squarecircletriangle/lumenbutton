package com.blocksglobal.lumen.button.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.blocksglobal.lumen.button.service.LumenBluetoothLeService;

/**
 * Created by nanik on 31/03/16.
 */
public class BootReceiver extends BroadcastReceiver {
    public static final String PACKAGE_NAME = "com.blocksglobal.lumen.button";
    public static final String INTENT_QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";

    private final String TAG = BootReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean executeService = false;
        String action = intent.getAction();
        String packageName=null;

        // check to make sure there data is available
        if (intent.getData()!=null) {
            // grab the package name
            packageName = intent.getData().getSchemeSpecificPart();

            // if the intent is package related than check to make sure
            // we only handle for our package to avoid re-execution of our service
            // everytime a package is installed/updated/replaced
            if (
                    (Intent.ACTION_PACKAGE_REPLACED.equals(action))   ||
                            (Intent.ACTION_PACKAGE_CHANGED.equals(action))
                    ) {
                if (packageName.equalsIgnoreCase(PACKAGE_NAME)) {
                    executeService = true;
                }
            }
        }

        // handle when the first time the device starts up
        if ( (Intent.ACTION_BOOT_COMPLETED.equals(action) ||
                (action.equalsIgnoreCase(INTENT_QUICKBOOT_POWERON)))) {
            executeService=true;
        }

        if (executeService) {
            Log.i(TAG, ">>>>>>>>>>>>>>>>>>> Starting LumenBluetoothLeService <<<<<<<<<<<<<<<<<<<<<<<<< ");
            context.startService(new Intent(context, LumenBluetoothLeService.class));
        }
    }
}
