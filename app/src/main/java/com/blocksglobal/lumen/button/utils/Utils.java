package com.blocksglobal.lumen.button.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.blocksglobal.lumen.button.service.LumenBluetoothLeService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by nanik on 1/04/16.
 */
public class Utils {
    static String TAG = LumenBluetoothLeService.class.getName();


    public static final  String HTTP_HEADER_AUTH = "Authorization";
    public static final  String HTTP_HEADER_AUTH_BEARER = "Bearer ";
    public static final  String HTTP_HEADER_CONTENT = "Content-Type";
    public static final  String HEADER_APPLICATION_JSON = "application/json";
    public static final  String HTTP_HEADER_ACCEPT = "Accept";
    public static final  String HTTP_POST = "POST";
    public static final  String incidentURL = "http://lumen-alert.blocksglobal.com/incidents";

    private static final int DEFAULT_COLUMN = 0;

    private final Handler handler = new Handler();

    public static   final String PROVIDER_CONTENT = "content://";
    public static   final String PROVIDER_AGENT_PACKAGE = "au.com.sct.agent/device";
    public static   final String PROVIDER_DEVICE_NAME = "/devicename";
    public static   final String PROVIDER_DEVICEID = "/id";
    public static   final String PROVIDER_EMAILID = "/emailid";

    // ContentProvider naming
    public static   final String PROVIDER_URI_DEVICE_NAME = PROVIDER_CONTENT + PROVIDER_AGENT_PACKAGE + PROVIDER_DEVICE_NAME;
    public static   final String PROVIDER_URI_DEVICEID = PROVIDER_CONTENT + PROVIDER_AGENT_PACKAGE + PROVIDER_DEVICEID;
    public static   final String PROVIDER_URI_EMAILID = PROVIDER_CONTENT + PROVIDER_AGENT_PACKAGE + PROVIDER_EMAILID;

    private static final String SCREENER_DEVICE_ID = "screener_device_id";
    private static final String authURL = "http://lumen.blocksglobal.com/api/v1/login/device";

    public  static final String JSON_TOKEN = "token";
    private String jsonToken = null;


    /**
     *
     * @param jsonkey
     * @param deviceId
     * @param deviceName
     */
    public static void postJson(String jsonkey, String deviceId, String deviceName) {
        JSONObject o = new JSONObject(initPostObject(deviceId, deviceName));
        String data = o.toString();
        String line = null;
        StringBuffer sb = new StringBuffer();

        if (jsonkey != null) {
            try {
                URL url = new URL(incidentURL);
                HttpURLConnection httpcon = (HttpURLConnection) ((url.openConnection()));
                httpcon.setDoOutput(true);
                httpcon.setDoInput(true);
                httpcon.setRequestProperty(HTTP_HEADER_AUTH, HTTP_HEADER_AUTH_BEARER + jsonkey);
                httpcon.setRequestProperty(HTTP_HEADER_CONTENT, HEADER_APPLICATION_JSON);
                httpcon.setRequestProperty(HTTP_HEADER_ACCEPT, HEADER_APPLICATION_JSON);
                httpcon.setRequestMethod(HTTP_POST);
                httpcon.connect();

                // post the data
                Log.i(TAG,"Posting data to server " + data);
                OutputStreamWriter out = new   OutputStreamWriter(httpcon.getOutputStream());
                out.write(data);
                out.close();

                int HttpResult=httpcon.getResponseCode();

                if(HttpResult==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpcon.getInputStream(),"utf-8"));
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                }
                Log.i(TAG, "Response after posting - responsecode = " +  sb.toString() + ".. data read = " + httpcon.getResponseMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param deviceId
     * @param deviceName
     * @return
     */
    private static HashMap initPostObject(String deviceId, String deviceName) {
        /**
         * Following is the JSON format to follow
         {
         severity: "warn",
         category: "userAction",
         slug: "help-requested",
         label: "Help Button Pressed",
         source_device_uuid: AuthService.getScreenerDeviceId(),
         source_device_name: AuthService.getScreenerDeviceNickName(),
         data: {}
         }
         */
        DateFormat df = new SimpleDateFormat("hh:mm a");

        HashMap l = new HashMap();
        l.put("severity", "warn");
        l.put("category","userAction");
        l.put("slug", "help-requested");
        l.put("label", "help-requested");
        l.put("source_device_uuid", deviceId);
        l.put("source_device_name",deviceName);
        l.put("severity","warn");
        l.put("data",new JSONObject());
        return l;
    }



    public static String getDeviceId(Context ctx) {
        return getInfofromAgent(ctx, PROVIDER_URI_DEVICEID);
    }

    /**
     *
     * @param context
     * @param uriString
     * @return
     */
    public static String getInfofromAgent(Context context, String uriString) {
        Cursor registrationInfo = context.getContentResolver()
                .query(Uri.parse(uriString), null, null, null, null);
        String val=null;

        if (registrationInfo == null) {
            return val;
        }
        if (registrationInfo.moveToFirst()) {
            val = registrationInfo.getString(DEFAULT_COLUMN);
        }
        registrationInfo.close();
        return val;
    }

    public static  String getEmailId(String provider_uri_emailid, Context ctx) {
        return getInfofromAgent(ctx, provider_uri_emailid);
    }

    public static String getDeviceName(String provider_uri_device_name, Context ctx) {
        return getInfofromAgent(ctx, provider_uri_device_name);
    }

    public static String getJsonTokenFromServer(final String emailparam, final String deviceidparam) throws MalformedURLException {
        URL url = new URL(authURL);

        StringBuffer response = null;
        HashMap<String, String> params = new HashMap<String, String>();

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailparam, deviceidparam.toCharArray());
            }
        });
        HttpURLConnection c = null;
        try {
            Log.i(TAG, "getJsonTokenFromServer...1");

            params.put(SCREENER_DEVICE_ID, deviceidparam);

            c = (HttpURLConnection) url.openConnection();
            c.setUseCaches(false);
            c.setRequestMethod("POST");
            c.setDoInput(true);
            c.setDoOutput(true);
            Log.i(TAG, "getJsonTokenFromServer...2");

            OutputStream os = c.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = c.getResponseCode();
            Log.i(TAG, "getJsonTokenFromServer...3");

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                Log.i(TAG, "getJsonTokenFromServer...4");

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                response = new StringBuffer();
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }

                Log.i(TAG, "getJsonTokenFromServer...5");

                String jsonToken = parseJsonToken(response.toString());
                response = new StringBuffer();
                response.append(jsonToken);

            } else {
                Log.i(TAG, "No response obtained from server. Response code = " + responseCode);
            }
        } catch (IOException e) {
            Log.i(TAG, "getJsonTokenFromServer...6");

            e.printStackTrace();
            response = null;
        } catch (JSONException e) {
            Log.i(TAG, "getJsonTokenFromServer...7");

            e.printStackTrace();
            response = null;
        }

        c.disconnect();
        Log.i(TAG, "*** getJsonTokenFromServer...8 RETURNING RESPONSE : " + response);

        return (response == null ? null : response.toString());

    }

    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    /**
     *
     * @param s
     * @return
     */
    public static String parseJsonToken(String s) throws JSONException {
        JSONObject obj = new JSONObject(s);
        return obj.getString(JSON_TOKEN);
    }
}
