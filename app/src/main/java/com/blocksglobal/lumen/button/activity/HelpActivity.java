package com.blocksglobal.lumen.button.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.blocksglobal.lumen.button.R;

public class HelpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setWindowUI();

    }

    private void setWindowUI() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.help_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Typeface font = Typeface.createFromAsset(getAssets(), "Raleway-Bold.ttf");
        TextView text = (TextView) findViewById(R.id.firstext);
        text.setTypeface(font);
        text.setTextColor(Color.BLACK);

        font = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        text = (TextView) findViewById(R.id.secondtext);
        text.setTypeface(font);
        text.setTextColor(Color.BLACK);

        ImageView phoneimage = (ImageView) findViewById(R.id.phoneimage);
        phoneimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
