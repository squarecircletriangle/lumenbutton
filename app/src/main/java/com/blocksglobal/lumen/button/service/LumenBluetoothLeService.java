/**
 * @author    Rajiv Manivannan <rajiv@contus.in>
 * @copyright  Copyright (C) 2014 VSNMobil. All rights reserved.
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 */
package com.blocksglobal.lumen.button.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.blocksglobal.lumen.button.Constants;
import com.blocksglobal.lumen.button.activity.HelpActivity;
import com.blocksglobal.lumen.button.executor.ProcessQueueExecutor;
import com.blocksglobal.lumen.button.executor.ReadWriteCharacteristic;
import com.blocksglobal.lumen.button.object.BluetoothDeviceObject;
import com.blocksglobal.lumen.button.utils.LogUtils;
import com.blocksglobal.lumen.button.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * BluetoothLeService.java
 *
 * The communication between the Bluetooth Low Energy device will be communicated through this service class only
 * The initial connect request and disconnect request will be executed in this class.Also, all the status from the Bluetooth device
 * will be notified in the corresponding callback methods.
 *
 */
public class LumenBluetoothLeService extends Service {
	public static final int STANDARD_TIMEOUT = 50000;
	public static final int DELAY_JSON_MILLIS = 2 * 1000;
	private String TAG = LumenBluetoothLeService.class.getName();

	// Constants going to use in the broadcast receiver as intent action.
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.vsnmobil.vsnconnect.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_GATT_CONNECTED = "com.vsnmobil.vsnconnect.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.vsnmobil.vsnconnect.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_DATA_RESPONSE = "com.vsnmobil.vsnconnect.ACTION_DATA_RESPONSE";


	public final static String EXTRA_DATA = "com.vsnmobil.vsnconnect.EXTRA_DATA";
	public final static String EXTRA_STATUS = "com.vsnmobil.vsnconnect.EXTRA_STATUS";
	public final static String EXTRA_ADDRESS = "com.vsnmobil.vsnconnect.EXTRA_ADDRESS";

	private  BluetoothManager bluetoothManager = null;
	private static BluetoothAdapter bluetoothAdapter = null;
	private static BluetoothGattService gattService = null;
	private BluetoothDevice device = null;
	public BluetoothGattCharacteristic mCharIdentify = null;
	public BluetoothGattCharacteristic mCharBlock = null;
	public BluetoothGattCharacteristic mCharVerification = null;

	public ProcessQueueExecutor processQueueExecutor=new ProcessQueueExecutor();
	public HashMap<String,BluetoothGatt> bluetoothGattMap;
	private BluetoothConnectionHandler mHandler = new BluetoothConnectionHandler();
	private boolean mScanning=false;
	private static final long SCAN_PERIOD = 10000;

	private ArrayList<BluetoothDeviceObject> btObject= new ArrayList<BluetoothDeviceObject>();
	private TextToSpeech testSpeech;
	private HashMap<String, String> map = new HashMap<String, String>();
	private boolean deviceFound =false;
	private boolean connectionOK = false;
	private Context ctx;
	public static final String BTNAME = "v.alrt";
	public static final String INCIDENT_URL = "http://lumen-alert.blocksglobal.com/incidents";
	private String jsonToken=null;
	private final Runnable jsonRunnable = new JSONTokenRunnable();
	private String deviceId=null;
	private String deviceName=null;

	class BluetoothConnectionHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
		}
	};


	BroadcastReceiver bState = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.i(TAG,">> Got intent  " + action);

			if(action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)){
				int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
						BluetoothAdapter.ERROR);
				switch (state){
					case BluetoothAdapter.STATE_OFF:
						Log.i(TAG,">>>>  BluetoothAdapter.STATE_OF");
						connectionOK=false;
						deviceFound=false;
						break;
					case BluetoothAdapter.STATE_TURNING_OFF:
						Log.i(TAG,">>>>  BluetoothAdapter.STATE_TURNING_OFF");
						connectionOK=false;
						deviceFound=false;
						break;
					case BluetoothAdapter.STATE_CONNECTED:
						Log.i(TAG,">>>>  BluetoothAdapter.STATE_CONNECTED");
						connectionOK=true;
						break;
					case BluetoothAdapter.STATE_ON:
						connectionOK=true;
						Log.i(TAG,">>>>  BluetoothAdapter.STATE_ON");
						break;
				}
			}
		}
	};

	/**
	 * runnable for scheduling JSONToken
	 */
	private void prepareJSONRunnable() {
		if (jsonToken==null) {
			mHandler.postDelayed(new Runnable() {
				public void run() {
					new Thread(jsonRunnable).start();
				}
			}, DELAY_JSON_MILLIS);
		}
	}

	/**
	 *
	 */
	private class JSONTokenRunnable implements Runnable {
		@Override
		public void run() {
			String emailid;
			String pwd=null;
			try {
				Log.i(TAG, "Getting JSON token ..");
				// grab the deviceid and emailid
				deviceId = Utils.getDeviceId(ctx);
				emailid = Utils.getEmailId(Utils.PROVIDER_URI_EMAILID, ctx);
				deviceName = Utils.getDeviceName(Utils.PROVIDER_URI_DEVICE_NAME, ctx);

				// retry to query from Agent again...
				if ((deviceId == null) || (emailid == null)) {
					Log.i(TAG, "Re-querying credentials...");
					deviceId = Utils.getDeviceId(ctx);
					emailid = Utils.getEmailId(Utils.PROVIDER_URI_EMAILID, ctx);
				}

				if (emailid!=null) {
					String split[] = emailid.split("@");
					if ((split != null) && (split.length >= 2)) {
						// when the email is split from test@email.com
						// the username/id will be in the 0th position
						pwd = split[0];
					}
				}

				// both deviceid and emailid MUST exist otherwise cannot authenticate
				if ((deviceId != null) && (emailid != null) && (pwd!=null) ) {
					Log.i(TAG, "--- JSONToken  deviceId = " +  deviceId + ".. deviceName = " + deviceName + ".. emailid = " + emailid);
					jsonToken = Utils.getJsonTokenFromServer(emailid,deviceId );
					Log.i(TAG, "--- JSONToken  obtained = " + jsonToken);
				} else {
					Log.d(TAG, "There is a problem obtaining Caldav crendentials. Aborting operation");
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				prepareJSONRunnable();
			}
		}
	}


	final Runnable runnable = new Runnable() {
		// Runnable for POSTing to the backend server
		final Runnable runnablePost  = new Runnable() {
			@Override
			public void run() {
				Log.i(TAG, "Posting JSON acknowlegement");
				Utils.postJson(jsonToken, deviceId, deviceName);
			}
		};

		@Override
		public void run() {
			// start the runnable to POST to backend server
			mHandler.postDelayed(new Runnable() {
				public void run() {
					new Thread(runnablePost).start();
				}
			}, 1);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG,"Initializing LumenBluetoothLeService...");
		ctx = super.getApplicationContext();

		setupReceivers();
		//if blue tooth adapter is not initialized stop the service.
//		if(isBluetoothEnabled(this)==false){
//			stopForeground(false);
//			LumenBluetoothLeService.this.stopSelf();
//		}
		// To add and maintain the BluetoothGatt object of each BLE device.
		bluetoothGattMap = new HashMap<String,BluetoothGatt>();
		//To execute the read and write operation in a queue.
		if (!processQueueExecutor.isAlive()) {
			processQueueExecutor.start();
		}

		initSpeech();
		checkBluetoothSettings();
		checkBluetoothConnectionState(30000);
		prepareJSONRunnable();
	}

	/**
	 *
	 * @param intent
	 * @param flags
	 * @param startId
	 * @return
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}


	/**
	 * BT call back
	 */
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
							 final byte[] scanRecord) {
			Log.i(TAG,"Found device = " + device.getName() + " .... " + device.getAddress());

			if (device.getName().toLowerCase().contains(BTNAME)) {
				Log.i(TAG,"Device already found so stopping the scanning proces..");
				addBTObject(device);
				bluetoothAdapter.stopLeScan(mLeScanCallback);
				connect(device.getAddress());
				deviceFound = true;
			}
		}
	};

	/**
	 * put device into our internal list
	 * @param device
	 */
	private void addBTObject(BluetoothDevice device) {
		BluetoothDeviceObject obj = new BluetoothDeviceObject();
		obj.setBtName(device.getName());
		obj.setBtAddress(device.getAddress());
		btObject.add(obj);
	}


	/**
	 * method to check bluetooth connection in loop
	 */
	public void checkBluetoothConnectionState(int time){
		Log.i(TAG,"1) CheckBluetoothConnectionState");

		if (time==0) {
			time = STANDARD_TIMEOUT;
		}

		if (!isBluetoothEnabled(ctx)) {
			bluetoothAdapter.enable();
		}

		// logic to check for bluetooth connection
		if ( connectionOK ) {
			Log.i(TAG, "2) Value of deviceFound = " + deviceFound);
			if (! deviceFound ) {
				if ((btObject != null) && (btObject.size() > 0)) {
					BluetoothDeviceObject btobj = btObject.get(0);
					Log.i(TAG, "3) Trying to reconnect to existing device");
					connect(btobj.getBtAddress());
				} else {
					Log.i(TAG, "4) Connection is OK so need to do scanning");
					scanLeDevice(true);
				}
			}
		}

		// re-run again
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				checkBluetoothConnectionState(30000);
			}
		},time);
	}

	/**
	 *
	 * @param enable
	 */
	private void scanLeDevice(final boolean enable) {
		if (enable) {
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.i(TAG,">>>> scanLeDevice  - stopping scanning process (1)");
					mScanning = false;
					bluetoothAdapter.stopLeScan(mLeScanCallback);
				}
			}, SCAN_PERIOD);

			Log.i(TAG,">>>> scanLeDevice  - starting scanning process");
			mScanning = true;
			bluetoothAdapter.startLeScan(mLeScanCallback);
		} else {
			Log.i(TAG,">>>> scanLeDevice  - stopping scanning process (2) ");
			mScanning = false;
			bluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}


	/**
	 *
	 */
	@Override
	public void onDestroy() {
		processQueueExecutor.interrupt();
		unregisterReceiver(bState);
	}

	/**
	 *
	 * @param rootIntent
	 */
	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
	}


	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/**
	 * Initializes a reference to the local Blue tooth adapter.
	 * @return Return true if the initialization is successful.
	 */
	public void setupReceivers() {
		IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		filter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
		registerReceiver(bState,filter);
	}

	/**
	 * Check bluetooth settings. Normally performed
	 * when the device starts up
	 */
	private void checkBluetoothSettings() {
		if (bluetoothManager == null) {
			bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (bluetoothManager == null) {
				Log.i(TAG, ">>> Not successfull obtaining Bluetooth Manager");
				return;
			}
		}

		bluetoothAdapter = bluetoothManager.getAdapter();
		if (bluetoothAdapter != null) {
			if (!bluetoothAdapter.isEnabled()) {
				bluetoothAdapter.enable();
			}
			connectionOK=true;

		}
	}

	/**
	 * To read the value from the BLE Device
	 * @param mGatt object of the device.
	 * @param characteristic of the device.
	 */
	public void readCharacteristic(final BluetoothGatt mGatt,final BluetoothGattCharacteristic characteristic) {
		if (!checkConnectionState(mGatt)) {
			return;
		}
		ReadWriteCharacteristic readWriteCharacteristic=new ReadWriteCharacteristic(ProcessQueueExecutor.REQUEST_TYPE_READ_CHAR, mGatt, characteristic);
		ProcessQueueExecutor.addProcess(readWriteCharacteristic);
	}

	/**
	 * To write the value to BLE Device
	 * @param mGatt object of the device.
	 * @param characteristic of the device.
	 * @param b value to write on to the BLE device.
	 */
	public void writeCharacteristic(final BluetoothGatt mGatt,final BluetoothGattCharacteristic characteristic, byte[] b) {
		if (!checkConnectionState(mGatt)) {
			return;
		}
		characteristic.setValue(b);
		ReadWriteCharacteristic readWriteCharacteristic=new ReadWriteCharacteristic(ProcessQueueExecutor.REQUEST_TYPE_WRITE_CHAR, mGatt, characteristic);
		ProcessQueueExecutor.addProcess(readWriteCharacteristic);
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 * @param characteristic Characteristic to act on.
	 * @param characteristic Characteristic to act on.
	 * @param enabled If true, enable notification. False otherwise.
	 */
	public void setCharacteristicNotification(final BluetoothGatt mGatt,BluetoothGattCharacteristic characteristic, boolean enabled) {
		if (!checkConnectionState(mGatt)) {
			return;
		}
		if (!mGatt.setCharacteristicNotification(characteristic, enabled)) {
			return;
		}
		final BluetoothGattDescriptor clientConfig = characteristic.getDescriptor(Constants.CLIENT_CHARACTERISTIC_CONFIG);
		if (clientConfig == null) {
			return;
		}
		clientConfig.setValue(enabled ? Constants.ENABLE_NOTIFICATION_VALUE: Constants.DISABLE_NOTIFICATION_VALUE);
		ReadWriteCharacteristic readWriteCharacteristic=new ReadWriteCharacteristic(ProcessQueueExecutor.REQUEST_TYPE_WRITE_DESCRIPTOR, mGatt, clientConfig);
		ProcessQueueExecutor.addProcess(readWriteCharacteristic);
	}

	/**
	 * Connects to the GATT server hosted on the Blue tooth LE device.
	 * @param address The device address of the destination device.
	 * @return Return true if the connection is initiated successfully. The connection result is reported asynchronously through the
	 * {@code BluetoothGattCallback# onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)} callback.
	 */
	public boolean connect(final String address) {
		if (bluetoothAdapter == null || address == null) {
			return false;
		}
		BluetoothGatt bluetoothGatt = bluetoothGattMap.get(address);
		if (bluetoothGatt != null) {
			bluetoothGatt.disconnect();
			bluetoothGatt.close();
		}
		device = bluetoothAdapter.getRemoteDevice(address);
		int connectionState = bluetoothManager.getConnectionState(device, BluetoothProfile.GATT);
		if (connectionState == BluetoothProfile.STATE_DISCONNECTED) {
			if (device == null) {
				return false;
			}
			// We want to directly connect to the device, so we are setting the
			// autoConnect parameter to false.
			BluetoothGatt mBluetoothGatt = device.connectGatt(this, false, mGattCallbacks);
			// Add the each BluetoothGatt in to an array list.
			if (!bluetoothGattMap.containsKey(address)) {
				bluetoothGattMap.put(address, mBluetoothGatt);
			} else {
				bluetoothGattMap.remove(address);
				bluetoothGattMap.put(address, mBluetoothGatt);
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * To disconnect the connected Blue tooth Low energy Device from the APP.
	 * @param gatt pass the GATT object of the device which need to be disconnect.
	 */
	public void disconnect(BluetoothGatt gatt) {
		if (gatt != null) {
			BluetoothDevice device = gatt.getDevice();
			String deviceAddress = device.getAddress();
			try {
				bluetoothGattMap.remove(deviceAddress);
				gatt.disconnect();
				gatt.close();
			} catch (Exception e) {
				LogUtils.LOGI(TAG,e.getMessage()) ;
			}
		}
	}

	/**
	 * To check the connection status of the GATT object.
	 * @param gatt pass the GATT object of the device.
	 * @return If connected it will return true else false.
	 */
	public boolean checkConnectionState(BluetoothGatt gatt) {
		if (bluetoothAdapter == null) {
			return false;
		}
		BluetoothDevice device = gatt.getDevice();
		String deviceAddress = device.getAddress();
		final BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(deviceAddress);
		int connectionState = bluetoothManager.getConnectionState(bluetoothDevice,BluetoothProfile.GATT);
		if (connectionState == BluetoothProfile.STATE_CONNECTED) {
			return true;
		}
		return false;
	}

	/**
	 * To check the connection status of the GATT object.
	 * @param deviceAddress MAC address of the device
	 * @return If connected it will return true else false.
	 */
	public boolean checkConnectionState(String deviceAddress) {
		if (bluetoothAdapter == null) {
			return false;
		}
		final BluetoothDevice btDevice = bluetoothAdapter.getRemoteDevice(deviceAddress);
		int connectionState = bluetoothManager.getConnectionState(btDevice, BluetoothProfile.GATT);
		if (connectionState == BluetoothProfile.STATE_CONNECTED) {
			return true;
		}
		return false;
	}
	// The connection status of the Blue tooth Low energy Device will be
	// notified in the below callback.
	private BluetoothGattCallback mGattCallbacks = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,int newState) {
			BluetoothDevice device = gatt.getDevice();
			String deviceAddress = device.getAddress();
			try {
				switch (newState) {
					case BluetoothProfile.STATE_CONNECTED:
						//start service discovery
						Log.i(TAG,"STATE_CONNECTED ");
						gatt.discoverServices();
						break;
					case BluetoothProfile.STATE_DISCONNECTED:
						try {
							Log.i(TAG,"STATE_DISCONNECTED ");
							bluetoothGattMap.remove(deviceAddress);
							gatt.disconnect();
							gatt.close();
						} catch (Exception e) {
							LogUtils.LOGI(TAG,e.getMessage()) ;
						}
						broadcastUpdate(ACTION_GATT_DISCONNECTED, deviceAddress,status);
						break;
					default:
						break;
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			BluetoothDevice device = gatt.getDevice();
			String deviceAddress = device.getAddress();
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_GATT_CONNECTED, deviceAddress, status);
				// Do APP verification as soon as service discovered.
				try {
					appVerification(gatt, getGattChar(gatt, Constants.SERVICE_VSN_SIMPLE_SERVICE,Constants.CHAR_APP_VERIFICATION),Constants.NEW_APP_VERIFICATION_VALUE);
				} catch (Exception e) {}

				for (BluetoothGattService service : gatt.getServices()) {

					Log.i(TAG,"Service = " + service.getUuid());
					if ((service == null) || (service.getUuid() == null)) {
						continue;
					}

					if (Constants.SERVICE_VSN_SIMPLE_SERVICE.equals(service.getUuid())) {
						mCharVerification =  service.getCharacteristic(Constants.CHAR_APP_VERIFICATION);
						// Writ Emergency key press 

						enableForDetect(gatt,service.getCharacteristic(Constants.CHAR_DETECTION_CONFIG),Constants.ENABLE_KEY_DETECTION_VALUE);

						// Set notification for emergency key press and fall detection
						setCharacteristicNotification(gatt,service.getCharacteristic(Constants.CHAR_DETECTION_NOTIFY),true);
						testSpeech.speak("..Alert Button Paired", TextToSpeech.QUEUE_FLUSH, map);
					} else if (Constants.Battery_Service_UUID.equals(service.getUuid())) {
//						Timer timer = new Timer("batteryTimer");
//						final BluetoothGatt s = gatt;
//						TimerTask task = new TimerTask() {
//							@Override
//							public void run() {
//								getBattery(s);
//							}
//						};
//						timer.scheduleAtFixedRate(task, 0, 5000);

						final BluetoothGatt s = gatt;
						mHandler.postDelayed(new Runnable() {
							public void run() {
								getBattery(s);
								mHandler.postDelayed(this, 5000);
							}
						}, 5000);
						enableForDetect(gatt,service.getCharacteristic(Constants.Battery_Level_UUID),Constants.ENABLE_KEY_DETECTION_VALUE);
					} else if (Constants.Link_Loss_UUID.equals(service.getUuid())) {
						BluetoothGattCharacteristic linkLossCharacter = service.getCharacteristic(Constants.Link_Loss_Characteristics);
						boolean b = gatt.readCharacteristic(linkLossCharacter);
						Log.i(TAG,linkLossCharacter.toString());
					}

				}
			} else {
				// Service discovery failed close and disconnect the GATT object of the device.
				gatt.disconnect();
				gatt.close();
			}
		}

		// CallBack when the response available for registered the notification( Battery Status, Fall Detect, Key Press)
		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic) {
			broadcastUpdate(ACTION_DATA_RESPONSE, characteristic.getUuid().toString(), "");
			Log.i(TAG,",characteristic.getUuid().toString() " + characteristic.getUuid().toString());

			if (characteristic.getUuid().equals(Constants.CHAR_DETECTION_NOTIFY)) {
				byte[] v = characteristic.getValue();
				byte value = v[0];
				if (value==1) {
					testSpeech.speak("Button Pressed", TextToSpeech.QUEUE_FLUSH, map);
					postIncident();
					launchHelp();
				}
			}

		}

		// Callback when the response available for Read Characteristic Request
		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic, int status) {
			Log.i(TAG,"onCharacteristicRead");

			if (characteristic.getUuid().equals(Constants.Battery_Level_UUID)) {
				byte[] v = characteristic.getValue();
				byte level = v[0];
				Log.i(TAG,"Battery level =" + level);
			}

		}

		// Callback when the response available for Write Characteristic Request
		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic, int status) {
			broadcastUpdate(ACTION_DATA_RESPONSE,characteristic.getUuid().toString(), status);
			Log.i(TAG,",characteristic.getUuid().toString() " + characteristic.getUuid().toString());
		}

		// Callback when the response available for Read Descriptor Request
		@Override
		public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,int status) {
			Log.i(TAG,"onDescriptorRead");
		}

		// Callback when the response available for Write Descriptor Request
		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,int status) {
			broadcastUpdate(ACTION_DATA_RESPONSE, "enabled key press event", status);
			Log.i(TAG,",characteristic.getUuid().toString() " + descriptor.getUuid().toString());


		}

		@Override
		public void onReadRemoteRssi(BluetoothGatt gatt, final int rssi,int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				String rssiValue = Integer.toString(rssi);
			}
		}
	};

	/**
	 *
	 */
	private void launchHelp() {
		Intent intent = new Intent();
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ComponentName cn = new ComponentName(this, HelpActivity.class);
		intent.setComponent(cn);
		startActivity(intent);
	}

	/**
	 * post to backend incident report
	 *
	 {
	 severity: "warn",
	 category: "userAction",
	 slug: "help-requested",
	 label: "Help Button Pressed",
	 source_device_uuid: AuthService.getScreenerDeviceId(),
	 source_device_name: AuthService.getScreenerDeviceNickName(),
	 data: {}
	 }
	 */
	private void postIncident() {
		new Thread(runnable).start();
	}


	/**
	 *
	 * @param gatt
	 */
	public void getBattery(BluetoothGatt gatt) {
		BluetoothGattService service = gatt.getService(Constants.Battery_Service_UUID);
		BluetoothGattCharacteristic batteryLevel = service.getCharacteristic(Constants.Battery_Level_UUID);
		gatt.readCharacteristic(batteryLevel);
	}

	/**
	 * To write the value to BLE Device for APP verification
	 * @param mGatt object of the device.
	 * @param ch of the device.
	 */
	public void appVerification(final BluetoothGatt mGatt, final BluetoothGattCharacteristic ch,final byte[] value) {
		writeCharacteristic(mGatt, ch, value);
	}

	/**
	 * To write the value to BLE Device for Emergency / Fall alert
	 * @param mGatt object of the device.
	 * @param ch of the device.
	 * @param value value to write on to the BLE device.
	 */
	public void enableForDetect(final BluetoothGatt mGatt, final BluetoothGattCharacteristic ch,final byte[] value) {
		writeCharacteristic(mGatt, ch, value);
	}

	/**
	 * To get the characteristic of the corresponding BluetoothGatt object and
	 * service UUID and Characteristic UUID.
	 * @param mGatt object of the device.
	 * @param serviceuuid UUID.
	 * @param charectersticuuid UUID.
	 * @return BluetoothGattCharacteristic of the given service and Characteristic UUID.  
	 */
	public  BluetoothGattCharacteristic getGattChar(BluetoothGatt mGatt, UUID serviceuuid,UUID charectersticuuid) {
		gattService = mGatt.getService(serviceuuid);
		return gattService.getCharacteristic(charectersticuuid);
	}

	/**
	 * To get the List of BluetoothGattCharacteristic from the given GATT object for Service UUID 
	 * @param mGatt object of the device.
	 * @param serviceuuid UUID.
	 * @return List of BluetoothGattCharacteristic.
	 */
	public List<BluetoothGattCharacteristic> getGattCharList(BluetoothGatt mGatt, UUID serviceuuid) {
		gattService = mGatt.getService(serviceuuid);
		return gattService.getCharacteristics();
	}
	/**
	 * To get the BluetoothGatt of the corresponding device
	 * @param bGattkey key value of hash map.
	 * @return BluetoothGatt of the device from the array
	 */
	public BluetoothGatt getGatt(String bGattkey) {
		return bluetoothGattMap.get(bGattkey);
	}

	/**
	 * Broadcast the values to the UI if the application is in foreground.
	 * @param action intent action.
	 * @param value value to update to the receiver.
	 */
	private void broadcastUpdate(final String action, final String value,final String address) {
		final Intent intent = new Intent(action);
		intent.putExtra(EXTRA_DATA, value);
		intent.putExtra(EXTRA_ADDRESS, address);
		sendBroadcast(intent);
	}

	/**
	 * Broadcast the values to the UI if the application is in foreground.
	 * @param action intent action.
	 * @param address address of the device.
	 * @param status connection status of the device.
	 */
	public void broadcastUpdate(final String action, final String address, final int status) {
		final Intent intent = new Intent(action);
		intent.putExtra(EXTRA_DATA, address);
		intent.putExtra(EXTRA_STATUS, status);
		sendBroadcast(intent);
	}

	/**
	 * To check the device bluetooth is enabled or not.
	 * @param context pass the context of your activity.
	 * @return boolean Bluetooth is enabled / disabled.
	 */
	public static boolean isBluetoothEnabled(Context context) {
		BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
		BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
		return bluetoothAdapter.isEnabled();
	}



	private void initSpeech() {
		testSpeech =new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				if(status != TextToSpeech.ERROR) {
					Log.i(TAG, ">>>> onInit initSpeech");
					testSpeech.setLanguage(Locale.ENGLISH);
				}
			}
		});

		testSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
			@Override
			public void onStart(String utteranceId) {
				Log.i(TAG, ">>>> onStart setOnUtteranceProgressListener");
			}

			@Override
			public void onDone(String utteranceId) {
				Log.i(TAG, ">>>> onDone setOnUtteranceProgressListener");
			}

			@Override
			public void onError(String utteranceId) {
				Log.i(TAG, ">>>> onError setOnUtteranceProgressListener");
			}
		});
		map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "LumenButton");
	}

}